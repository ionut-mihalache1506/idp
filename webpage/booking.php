<?php
    session_start();
?>

<!DOCTYPE html>

<head>
    <title>ProAir</title>
    <link rel="shortcut icon" href="logo.ico" />

    <!-- Style files -->
    <link href="styles/main.css" rel="stylesheet" />
    <link href="styles/blocks.css" rel="stylesheet" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function (){
            $('#tara').on('change', function() {
                var id_tara = $(this).val();
                if (id_tara) {
                    $.ajax({
                        type: 'POST',
                        url: 'states.php',
                        data: 'id_tara=' + id_tara,
                        success: function(html) {
                            $('#oras').html(html);
                        }
                    });
                } else {
                    $('#oras').html("<option value='None' selected disabled hidden>Alegeți mai întâi o țară din care plecați</option>");
                }
            });
        });
    </script>

    <script>
        $(document).ready(function (){
            $('#tara1').on('change', function() {
                var id_tara = $(this).val();
                if (id_tara) {
                    $.ajax({
                        type: 'POST',
                        url: 'states.php',
                        data: 'id_tara=' + id_tara,
                        success: function(html) {
                            $('#oras1').html(html);
                        }
                    });
                } else {
                    $('#oras1').html("<option value='None' selected disabled hidden>Alegeți mai întâi o țară din care plecați</option>");
                }
            });
        });
    </script>

</head>

<body>

<div class='body'>
    <div class='menu'>
        <div class='cname'>
            <span id='pro'>PRO</span><span id='air'>AIR</span>
        </div>

        <div id='home' onclick="window.location.href = 'index.php'">
            <p><a href="index.php">Home</a></p>
        </div>

    <?php
        if (@$_SESSION['uname']) {
            echo "\t<div id='bticket' onclick=\"window.location.href = 'booking.php'\">\n";
            echo "\t\t<p>Book ticket</p>\n";
            echo "\t</div>\n";
        }
    ?>

    <?php
        if (@!$_SESSION['uname']) {
            echo "\t<div id='login' onclick=\"window.location.href = 'formular.php'\">\n";
            echo "\t\t<p>Login</p>\n";
            echo "\t</div>\n";
        }
    ?>
    <?php
        if (@$_SESSION['uname']) {
            $username = $_SESSION['uname'];
            $nume = $_SESSION['nume'];
            $prenume = $_SESSION['prenume'];
            echo "<div id='ureg1'>";
            echo "<p>Logged in as $username($prenume $nume)</p>\n";
            echo "</div>";
        } else {
            echo "<div id='ureg'>";
            echo "<p>Not registered</p>\n";
            echo "</div>";
        }
    ?>

    <?php
        if (@$_SESSION['uname']) {
            echo "<form name='formular' action='' method='post' class='fomular'>";
            echo '<input type="submit" name="logout_button" id="logout_button" value="Logout">';
            echo "</form>";
            if (isset($_POST['logout_button'])) {
                session_destroy();
    ?>
    <script type="text/javascript">
        window.location.href = 'index.php';
    </script>
    <?php
            }
        }
    ?>
    </div>

    <div class='content'>
        <div id="form">
            <p>Book ticket</p>
            <form name="formular_booking" id="formular_booking" action="" method="post" class='fomular'>
                <span>Plecare:</span>
                <select name='tara' id='tara'>
                    <option value='None' disabled selected hidden>Alegeți țara din care plecați</option>
                    <?php
                        $hostname = "mysql";
                        $username = "admin";
                        $password = "admin";
                        $dbname = "proairdb";
                    
                        $link = mysqli_connect($hostname, $username, $password, $dbname);
                    
                        if (!$link) {
                            echo "Eroare: Nu a fost posibilă conectarea la MySQL." . PHP_EOL;
                            echo "Valoarea errno: " . mysqli_connect_errno() . PHP_EOL;
                            echo "Valoarea error: " . mysqli_connect_error() . PHP_EOL;
                            exit;
                        }

                        $str_query = "call getCountries()";
                        if ($result = mysqli_query($link, $str_query)) {

                            while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                $id = strval($row['ID']);
                                echo "<option value='$id'>";
                                echo $row['nume'];
                                echo "</option><br>";
                            }
                            mysqli_free_result($result);
                        }
            
                        // Closing connection
                        mysqli_close($link);
                    ?>
                </select>
                <select name='oras' id='oras'>
                    <option value='None' selected disabled>Alegeți mai întâi o țară din care plecați</option>
                </select>

                <span>Sosire:</span>
                <select name='tara1' id='tara1' style="margin-left: 1.1%;">
                    <option value='None' disabled selected hidden>Alegeți țara unde doriți să ajungeți</option>
                    <?php
                        $hostname = "mysql";
                        $username = "admin";
                        $password = "admin";
                        $dbname = "proairdb";
                     
                        $link = mysqli_connect($hostname, $username, $password, $dbname);
                     
                        if (!$link) {
                            echo "Eroare: Nu a fost posibilă conectarea la MySQL." . PHP_EOL;
                            echo "Valoarea errno: " . mysqli_connect_errno() . PHP_EOL;
                            echo "Valoarea error: " . mysqli_connect_error() . PHP_EOL;
                            exit;
                        }

                        mysqli_autocommit($link, TRUE);
 
                        $str_query = "call getCountries()";
                        if ($result = mysqli_query($link, $str_query)) {
 
                            while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                $id = strval($row['ID']);
                                echo "<option value='$id'>";
                                echo $row['nume'];
                                echo "</option>\n";
                            }
                            mysqli_free_result($result);
                         }
             
                         // Closing connection
                         mysqli_close($link);
                    ?>
                </select>
                <select name='oras1' id='oras1'>
                    <option value='None' selected disabled>Alegeți mai întâi o țară unde doriți să ajungeți</option>
                </select>
                <span>Dată și oră de plecare:</span>
                <input type="datetime-local" id="data_plecare" name="data_plecare" value=<?php date_default_timezone_set('Europe/Bucharest'); echo date('Y-m-d\TH:i:s'); ?> step="1">
                <input type="submit" name="show_tickets_button" id="show_tickets_button" value="Show tickets">
            </form>
        </div>
        <?php
            if (isset($_POST['show_tickets_button'])) {
                $hostname = "mysql";
                $username = "admin";
                $password = "admin";
                $dbname = "proairdb";
                     
                $link = mysqli_connect($hostname, $username, $password, $dbname);
                     
                if (!$link) {
                    echo "Eroare: Nu a fost posibilă conectarea la MySQL." . PHP_EOL;
                    echo "Valoarea errno: " . mysqli_connect_errno() . PHP_EOL;
                    echo "Valoarea error: " . mysqli_connect_error() . PHP_EOL;
                    exit;
                }
                mysqli_autocommit($link, TRUE);

                $username = $_SESSION["uname"];
                $nume = $_SESSION["nume"];
                $prenume = $_SESSION["prenume"];
                $data = $_POST["data_plecare"];
                $dt = new DateTime($data);
                $data_plecare = strval($dt->format("d")."-".strtoupper($dt->format("M"))."-".$dt->format("Y"));
                $ora_plecare = intval($dt->format("H"));
                $id_tara_plecare = $_POST['tara'];
                $id_oras_plecare = $_POST['oras'];
                $id_tara_sosire = $_POST['tara1'];
                $id_oras_sosire = $_POST['oras1'];

                $query_str = "call getFlights($id_tara_plecare, $id_oras_plecare,
                                              $id_tara_sosire, $id_oras_sosire,
                                              '$data', $ora_plecare)";
                if ($result = mysqli_query($link, $query_str)) {
                    $ncols = mysqli_field_count($link);
                    echo "<form method='post' action='' name='buy_form' id='buy_form'>";
                    echo "<table class='header'>\n";
                    echo "\t<tr>\n";
    
                    // Print table header
                    for ($i = 0; $i < $ncols; ++$i) {
                        $finfo = mysqli_fetch_field_direct($result, $i);
                        $column_name = strtoupper($finfo->name);
                        echo "\t\t<td>$column_name</td>\n";
                    }
                    echo "\t\t<td>CUMPARA BILET</td>\n";
                    echo "\t</tr>\n";

                    $r = 0;
                    while (($row = mysqli_fetch_array($result, MYSQLI_BOTH)) != False) {
                        $row_name = strval($r);
                        echo "\t<tr name = '$row_name'>\n";
                        $c = 0;
                        while ($c < $ncols) {
                            $finfo = mysqli_fetch_field_direct($result, $c);
                            $column_name = $finfo->name;
                            $col_name = $column_name.strval($r);
                            $val = $row[$c] == null ? "" : $row[$c];
                            echo "\t\t<td><input type='hidden' name='$col_name' value='$val'>$val</td>\n";
                            $c++;
                        }
                        $btn_name = strval("buy_ticket_button[".$row_name."]");
                        echo "\t\t<td><input type='submit' name='$btn_name' id='buy_ticket_button' value='Cumpără' onclick='window.location.href = \'index.php\'></td>\n";
                        echo "\t</tr>\n";
                        $r++;
                    }
                    echo "</table>\n";
                    echo "</form>";
                    mysqli_free_result($result);
                } else {
                    echo mysqli_error($link);
                }
                // Closing connection
                mysqli_close($link);
            }
            if (!empty(@$_POST['buy_ticket_button'])) {
                $hostname = "mysql";
                $username = "admin";
                $password = "admin";
                $dbname = "proairdb";
                     
                $link = mysqli_connect($hostname, $username, $password, $dbname);

                mysqli_autocommit($link, TRUE);

                if (!$link) {
                    echo "Eroare: Nu a fost posibilă conectarea la MySQL." . PHP_EOL;
                    echo "Valoarea errno: " . mysqli_connect_errno() . PHP_EOL;
                    echo "Valoarea error: " . mysqli_connect_error() . PHP_EOL;
                    exit;
                }

                $r = key(@$_POST['buy_ticket_button']);
                $key = 'ID'.strval($r);
                $id_zbor = $_POST[$key];
                $id_client = $_SESSION['cnp'];

                $str_query = "call insertTicket($id_zbor, '$id_client')";
                mysqli_query($link, $str_query);

                mysqli_close($link);
            }
        ?>
    </div>

    <div class='footer'>
    </div>
</div>

</body>

</html>
