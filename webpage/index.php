<?php
    session_start();
?>

<!DOCTYPE html>

<head>
    <title>ProAir</title>
    <link rel="shortcut icon" href="logo.ico" />

    <!-- Style files -->
    <link href="styles/main.css" rel="stylesheet" />
    <link href="styles/blocks.css" rel="stylesheet" />

</head>

<body>

<div class='body'>

<div class='menu'>
    <div class='cname'>
        <span id='pro'>PRO</span><span id='air'>AIR</span>
    </div>

    <div id='home' onclick="window.location.href = 'index.php'">
        <p><a href="index.php">Home</a></p>
    </div>

    <?php
        if (@$_SESSION['uname'] && @$_SESSION['uname'] != "admin") {
            echo "\t<div id='bticket' onclick=\"window.location.href = 'booking.php'\">\n";
            echo "\t\t<p>Book ticket</p>\n";
            echo "\t</div>\n";
        }
    ?>

    <?php
        if (@!$_SESSION['uname']) {
            echo "\t<div id='login' onclick=\"window.location.href = 'formular.php'\">\n";
            echo "\t\t<p>Login</p>\n";
            echo "\t</div>\n";
        }
    ?>
    <?php
        if (@$_SESSION['uname']) {
            $username = $_SESSION['uname'];
            $nume = $_SESSION['nume'];
            $prenume = $_SESSION['prenume'];
            echo "<div id='ureg1'>";
            echo "<p>Logged in as $username($prenume $nume)</p>\n";
            echo "</div>";
        } else {
            echo "<div id='ureg'>";
            echo "<p>Not registered</p>\n";
            echo "</div>";
        }
    ?>

    <?php
        if (@$_SESSION['uname']) {
            echo "<form name='formular' action='' method='post' class='fomular'>";
            echo '<input type="submit" name="logout_button" id="logout_button" value="Logout">';
            echo "</form>";
            if (isset($_POST['logout_button'])) {
                session_destroy();
    ?>
    <script type="text/javascript">
        window.location.href = 'index.php';
    </script>
    <?php
            }
        }
    ?>
</div>

<div class='content' style="height: 79%">
<?php

    // Creating connection to the database
    if (isset($_GET['submitButton'])) {
        header("Location: formular.php");
    }

    $hostname = "mysql";
    $username = "admin";
    $password = "admin";
    $dbname = "proairdb";

    $link = mysqli_connect($hostname, $username, $password, $dbname);

    if (!$link) {
        echo "Eroare: Nu a fost posibilă conectarea la MySQL." . PHP_EOL;
        echo "Valoarea errno: " . mysqli_connect_errno() . PHP_EOL;
        echo "Valoarea error: " . mysqli_connect_error() . PHP_EOL;
        exit;
    }

    mysqli_autocommit($link, TRUE);

    if (@$_SESSION['uname'] == "admin") {
        echo '<iframe src="http://localhost:8081" style="height:100%;width:100%;" scrolling="yes" frameBorder="0"></iframe>';
    } else {
        if (@$_SESSION['cnp']) {
            $id_client = $_SESSION['cnp'];
            $str_query = "call getTickets('$id_client')";
            
            if ($result = mysqli_query($link, $str_query)) {
                $ncols = mysqli_field_count($link);
                echo "<table class='header1'>\n";
                echo "\t<tr>\n";

                // Print table header
                for ($i = 0; $i < $ncols; ++$i) {
                    $finfo = mysqli_fetch_field_direct($result, $i);
                    $column_name = strtoupper($finfo->name);
                    echo "\t\t<td>$column_name</td>\n";
                }

                while (($row = mysqli_fetch_array($result, MYSQLI_BOTH)) != False) {
                    echo "\t<tr>\n";
                    $c = 0;
                    while ($c < $ncols) {
                        $finfo = mysqli_fetch_field_direct($result, $c);
                        $column_name = $finfo->name;
                        $col_name = $column_name.strval($r);
                        $val = $row[$c] == null ? "" : $row[$c];
                        echo "\t\t<td>".$val."</td>\n";
                        $c++;
                    }
                    echo "\t</tr>\n";
                }

                echo "\t</tr>\n";
                echo "</table>\n";

                mysqli_free_result($result);
            } else {
                echo mysqli_error($link);
            }
        }
    }

    // Closing connection
    mysqli_close($link);
?>
</div>

</div>

<div class='footer'>
</div>

</body>

</html>
