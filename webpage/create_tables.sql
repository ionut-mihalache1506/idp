create table clienti(
        ID varchar2(15) constraint pk_id_client primary key,
        uname varchar2(255) unique,
        nume varchar2(255),
        prenume varchar2(255),
        parola varchar2(255)
);

create table tari (
        ID number constraint pk_id_tara primary key,
        nume VARCHAR2(255)
);

create table orase (
        ID number constraint pk_id_oras primary key,
        id_tara number constraint fk_id_tara references tari(ID),
        nume VARCHAR2(255)
);

create table zboruri (
        ID number constraint pk_id_zbor primary key,
        id_tara_plecare number constraint fk_id_tara_plecare references tari(ID),
        id_oras_plecare number constraint fk_id_oras_plecare references orase(ID),
        id_tara_sosire number constraint fk_id_tara_sosire references tari(ID),
        id_oras_sosire number constraint fk_id_oras_sosire references orase(ID),
        data_plecare date,
        ora_plecare number,
        durata number,
        clasa number,
        nr_locuri number,
        pret number(7, 2)
);

create table bilete (
        ID number constraint pk_id_bilet primary key AUTO_INCREMENT,
        id_zbor number constraint fk_id_zbor references zboruri(ID),
        id_client varchar2(13) constraint fk_id_client references clienti(ID)
);
ALTER TABLE bilete AUTO_INCREMENT=0;
