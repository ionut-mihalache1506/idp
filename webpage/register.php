<?php
    session_start();
?>

<!DOCTYPE html>

<head>
    <title>ProAir</title>
    <link rel="shortcut icon" href="logo.ico" />

    <!-- Style files -->
    <link href="styles/main.css" rel="stylesheet" />
    <link href="styles/blocks.css" rel="stylesheet" />

</head>

<body>

<div class='body'>
    <div class='menu'>
        <div class='cname'>
            <span id='pro'>PRO</span><span id='air'>AIR</span>
        </div>

        <div id='home' onclick="window.location.href = 'index.php'">
            <p><a href="index.php">Home</a></p>
        </div>

        <div id='login' onclick="window.location.href = 'formular.php'">
            <p>Login</p>
        </div>
    </div>

    <div class='content'>
        <div id="form">
            <p>Register form</p>
            <form name="formular" action="" method="post" class='fomular'>
                <input type="text" name="nume" id="nume" placeholder = 'Nume' onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nume'">
                <input type="text" name="prenume" id="prenume" placeholder = 'Prenume' onfocus="this.placeholder = ''" onblur="this.placeholder = 'Prenume'">
                <input type="password" name="pass" id="password" placeholder = 'Password' onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">
                <input type="text" name="cnp" id="password" placeholder = 'CNP' onfocus="this.placeholder = ''" onblur="this.placeholder = 'CNP'">
                <input type="text" name="uname" id="username" placeholder = 'Username' onfocus="this.placeholder = ''" onblur="this.placeholder = 'Username'">
                <input type="submit" name="login_button" id="login_button" value="Register">
            </form>
        </div>

        <?php
            if (isset($_POST['login_button'])) {
                $hostname = "mysql";
                $username = "admin";
                $password = "admin";
                $dbname = "proairdb";

                $link = mysqli_connect($hostname, $username, $password, $dbname);

                if (!$link) {
                    echo "Eroare: Nu a fost posibilă conectarea la MySQL." . PHP_EOL;
                    echo "Valoarea errno: " . mysqli_connect_errno() . PHP_EOL;
                    echo "Valoarea error: " . mysqli_connect_error() . PHP_EOL;
                    exit;
                }

                mysqli_autocommit($link, TRUE);
                
                $username = $_POST["uname"];
                $password = hash('sha256', $_POST["pass"]);
                $nume = $_POST["nume"];
                $prenume = $_POST["prenume"];
                $cnp = $_POST["cnp"];

                $str_query = "CALL insertClient('$cnp', '$username', '$nume', '$prenume', '$password')";

                if (!mysqli_query($link, $str_query)) {
                    echo mysqli_error($link);
                }

                $_SESSION['uname'] = $username;
                $_SESSION['nume'] = $nume;
                $_SESSION['prenume'] = $prenume;
                $_SESSION['cnp'] = $cnp;
            
        ?>
            <script type="text/javascript">
                window.location.href = 'index.php';
            </script>
        <?php
                // Closing connection
                mysqli_close($link);
            }
        ?>

    </div>
</div>

</body>

</html>
