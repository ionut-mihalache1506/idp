set serveroutput on;
SET TRIMSPOOL ON;
SET TRIMOUT ON;
SET LINESIZE 2000;


drop sequence seq_bilete;
drop trigger bilete_before_insert;

drop table bilete;
drop table clienti;
drop table zboruri;
drop table orase;
drop table tari;

-- Create tables
@@create_tables.sql

-- Create triggers
@@create_triggers.sql

-- Create functions
@@create_functions.sql

-- Create procedures
@@create_procedures.sql

insert into clienti
values ('0000000000000', 'admin', 'Min', 'Ad', 'admin');

-- insert into clienti
-- values ('000000000001', 'Ionut Mihalache', 'Mihalache', 'Ionut', '1234');

insert into tari 
values (0, 'Romania');
insert into tari
values(1, 'Statele Unite ale Americii');

insert into orase
values (0, 0, 'Bucuresti');
insert into orase
values (1, 0, 'Cluj');
insert into orase
values (2, 0, 'Timisoara');

insert into orase
values (3, 1, 'Chicago');
insert into orase
values (4, 1, 'New York');
insert into orase
values (5, 1, 'Washington DC');

insert into zboruri
values (0, 0, 0, 1, 3, '07-JAN-2020', 5, 10, 1, 150, 1000);
insert into zboruri
values (1, 0, 0, 1, 4, '08-JAN-2020', 10, 10, 1, 50, 1200);
insert into zboruri
values (2, 0, 2, 1, 5, '09-JAN-2020', 12, 10, 1, 100, 1000);
insert into zboruri
values (3, 0, 0, 1, 3, '10-JAN-2020', 5, 10, 1, 100, 1000);
insert into zboruri
values (4, 0, 0, 1, 3, '11-JAN-2020', 5, 10, 1, 80, 1000);
insert into zboruri
values (5, 0, 0, 1, 3, '12-JAN-2020', 8, 15, 2, 150, 100);
insert into zboruri
values (6, 0, 0, 0, 1, '15-JAN-2020', 8, 15, 2, 100, 50);

select * from clienti;
select * from tari;
select * from orase;
select * from zboruri;
select * from bilete;

select id_zbor, count(id_zbor) from bilete group by id_zbor having count(id_zbor) = (select max(count(id_zbor)) from bilete group by id_zbor);
