create sequence seq_bilete;
create or replace trigger bilete_before_insert
before insert on bilete
for each row
declare
    nr_loc number;
begin
        update
            zboruri
        set
            nr_locuri = nr_locuri - 1
        where
            id = :new.id_zbor;

        select
                seq_bilete.nextval - 1
        into
                :new.id
        from
                dual;
end bilete_before_insert;
/
show errors;
