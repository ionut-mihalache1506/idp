<?php

if (isset($_POST['id_tara'])) {
    $hostname = "mysql";
    $username = "admin";
    $password = "admin";
    $dbname = "proairdb";

    $link = mysqli_connect($hostname, $username, $password, $dbname);

    if (!$link) {
        echo "Eroare: Nu a fost posibilă conectarea la MySQL." . PHP_EOL;
        echo "Valoarea errno: " . mysqli_connect_errno() . PHP_EOL;
        echo "Valoarea error: " . mysqli_connect_error() . PHP_EOL;
        exit;
    }

    $id_tara = $_POST['id_tara'];

    $str_query = "call getStates($id_tara)";

    if ($result = mysqli_query($link, $str_query)) {

        echo "<option value='' selected disabled hidden>Alegeți orașul</option>";
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            $id = strval($row['id']);
            echo "<option value='$id'>";
            echo $row['nume'];
            echo "</option>";
        }
        mysqli_free_result($result);
    }

    // Closing connection
    mysqli_close($link);
}

?>
