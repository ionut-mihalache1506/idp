drop procedure insertClient;
drop procedure insertTicket;
drop procedure getStates;

create or replace procedure insertClient(cnp in varchar2, username in varchar2,
                                         nume in varchar2, prenume in varchar2,
                                         password in varchar2)
is
begin
        insert into clienti values(cnp, username, nume, prenume, password);
end insertClient;
/
show errors;

create or replace procedure insertTicket(id_z in number, id_c in varchar2)
is
begin
        insert into bilete (id_zbor, id_client) values(id_z, id_c);
end;
/
show errors;

create or replace procedure getStates(id_t in number, res out sys_refcursor)
is
begin
        open res for select id, nume from orase where id_tara = id_t;
end getStates;
/
show errors;
