drop function getTickets;
drop function getCountries;
drop function getCredentials;
drop function getFlights;
drop function getMostVisitedCountry;

create or replace function getTickets(id_c number)
return sys_refcursor
is
        res sys_refcursor;
begin
        open res for
        select
                t1.nume tara_plecare,
                o1.nume oras_plecare,
                t2.nume tara_sosire,
                o2.nume oras_sosire,
                c.nume,
                c.prenume,
                z.data_plecare,
                z.ora_plecare,
                z.durata "DURATA (ORE)",
                z.clasa,
                z.pret "PRET (EURO)"

        from
                bilete b,
                zboruri z,
                clienti c,
                tari t1,
                orase o1,
                tari t2,
                orase o2

        where
                id_client = id_c and
                c.id = id_c and
                b.id_zbor = z.id and
                z.id_tara_plecare = t1.id and
                z.id_oras_plecare = o1.id and
                z.id_tara_sosire = t2.id and
                z.id_oras_sosire = o2.id;
        
        return res;
end getTickets;
/
show errors;

create or replace function getCountries
return sys_refcursor
is
        res sys_refcursor;
begin
        open res for select * from tari;
        return res;
end getCountries;
/
show errors;

create or replace function getCredentials(username varchar2,
                                          password varchar2)
return sys_refcursor
is
        res sys_refcursor;
begin
        open res for select * from clienti where uname = username and parola = password;
        return res;
end getCredentials;
/
show errors;

create or replace function getFlights(id_tp number, id_op number,
                                      id_ts number, id_os number,
                                      datap date, orap number)
return sys_refcursor
is
        res sys_refcursor;
begin
        open res for
        select
                z.ID,
                t.nume tara_plecare,
                o.nume oras_plecare,
                t1.nume tara_sosire,
                o1.nume oras_sosire,
                to_char(z.data_plecare, 'dd-mm-yyyy') data_plecare,
                z.ora_plecare,
                z.durata,
                z.clasa,
                z.nr_locuri,
                z.pret

        from
                zboruri z,
                tari t,
                tari t1,
                orase o,
                orase o1

        where
                z.id_tara_plecare = id_tp and t.id = id_tp and
                z.id_oras_plecare = id_op and o.id = id_op and
                z.id_tara_sosire = id_ts and t1.id = id_ts and
                z.id_oras_sosire = id_os and o1.id = id_os and
                z.nr_locuri > 0 and
                (z.data_plecare > datap or (z.data_plecare = datap and z.ora_plecare > orap));
        
        return res;
end getFlights;
/
show errors;

create or replace function getMostVisitedCountry(max_nr_visits out number)
return varchar2
is
        res varchar2(1000) := '';
        aux varchar2(500);
        most_tickets number;
        flight_id number;
begin
        select max(count(id_zbor)) into most_tickets from bilete group by id_zbor;
        max_nr_visits := most_tickets;
        for zbor in (select id_zbor from bilete group by id_zbor having count(id_zbor) = most_tickets) loop       
                select t.nume into aux from zboruri z, tari t where z.id = zbor.id_zbor and z.id_tara_sosire = t.id;
                if res is null then
                        res := res || aux;
                else
                        res := res || ',' || aux;
                end if;
        end loop;
        return res;
end getMostVisitedCountry;
/
show errors;
