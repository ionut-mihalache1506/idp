#!/usr/bin/python

import mysql.connector
import socket
import os
import time

HOST = 'admin_server'     # The server's hostname or IP address
PORT = 65432        # The port used by the server
RECV_BUFSIZE = 4096

config = {
    'user': 'admin',
    'password': 'admin',
    'host': 'mysql',
    'port': 3306,
    'database': 'proairdb'
}

ok = False
mydb = None
while not ok:
    try:
        mydb = mysql.connector.connect(**config)
        ok = True
    except Exception as e:
        print(e)
        continue

mydb.start_transaction(isolation_level='READ COMMITTED')
mycursor = mydb.cursor()

print("Welcome to admin client!")
print("Attention! Every operation you make will be logged!!!")

def config_db(s):
    print("Use help command if this is your first time using this program")
    while True:
        sqlcmd = input()
        cmds = sqlcmd.split(' ')

        if cmds[0] == "help":
            print("help")
            print("show <table name>")
            print("insert <table name> <values>")
            print("delete <table name> <id_name> <id_value>")

        if cmds[0] == "show":
            showcmd = "select * from " + cmds[1] + ";"

            s.send(showcmd.encode())
            mycursor.execute(showcmd)

            for row in mycursor:
                print(row)

            continue

        if cmds[0] == "insert":
            insertcmd = "insert into " + cmds[1] + " values("

            i = 2
            while i < (len(cmds) - 1):
                insertcmd += cmds[i] + ", "
                i += 1
            insertcmd += cmds[-1] + ');'

            s.send(insertcmd.encode())
            mycursor.execute(insertcmd)
            mydb.commit()

            print('Data inserted')

            continue

        if cmds[0] == "delete":
            deletecmd = "delete from " + cmds[1] + " where " + cmds[2] + " = " + cmds[3] + ";"

            s.send(deletecmd.encode())
            mycursor.execute(deletecmd)
            mydb.commit()

            print("Data deleted")

            continue

        if cmds[0] == "exit":
            print("Admin client connection is closing")
            s.send(cmds[0].encode())
            return 1
        if cmds[0] == "logout":
            print("Admin client is closing")
            s.send(cmds[0].encode())
            return -1
    s.close()
    return 0

ok = False
while not ok:
    # Communication with server
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))

            data = s.recv(RECV_BUFSIZE)
            print(data.decode())
            print("Give command: ", end = '')
            server_cmd = input()
            s.send(server_cmd.encode())
            if server_cmd == "logout":
                break
            if server_cmd != "login":
                break
            
            # Username check
            data = s.recv(RECV_BUFSIZE)
            print(data.decode(), end = '')
            username = input()
            s.send(username.encode())
            data = s.recv(RECV_BUFSIZE)
            response = data.decode()
            if "not" in response:
                print(response)
                ok = True
                break

            # Password check
            print(response, end = '')
            password = input()
            s.send(password.encode())
            data = s.recv(RECV_BUFSIZE)
            response = data.decode()
            if "Wrong" in response:
                print(response)
                ok = True
                break
            print(response)
            ret = config_db(s)
            if ret < 0:
                ok = True
    except:
        continue

mydb.close()
