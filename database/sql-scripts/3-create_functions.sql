-- drop function getTickets;
-- drop function getCountries;
-- drop function getCredentials;
-- drop function getFlights;
-- drop function getMostVisitedCountry;

delimiter $$
create procedure getTickets(id_c varchar(15))
begin
    select
        t1.nume tara_plecare,
        o1.nume oras_plecare,
        t2.nume tara_sosire,
        o2.nume oras_sosire,
        c.nume,
        c.prenume,
        z.data_plecare,
        z.ora_plecare,
        z.durata "durata (ore)",
        z.clasa,
        z.pret "pret (euro)"
    from
        bilete b,
        zboruri z,
        clienti c,
        tari t1,
        orase o1,
        tari t2,
        orase o2
    where
        id_client = id_c and
        c.id = id_c and
        b.id_zbor = z.id and
        z.id_tara_plecare = t1.id and
        z.id_oras_plecare = o1.id and
        z.id_tara_sosire = t2.id and
        z.id_oras_sosire = o2.id;
end $$
delimiter ;

delimiter $$
create procedure getCountries()
begin
    select * from tari;
end $$
delimiter ;

delimiter $$
create procedure getCredentials(username varchar(255),
                                password varchar(255))
begin
        select * from clienti where uname = username and parola = password;
end $$
delimiter ;

delimiter $$
create procedure getFlights(id_tp int, id_op int,
                            id_ts int, id_os int,
                            datap date, orap int)
begin
    select
        z.ID,
        t.nume tara_plecare,
        o.nume oras_plecare,
        t1.nume tara_sosire,
        o1.nume oras_sosire,
        z.data_plecare data_plecare,
        z.ora_plecare,
        z.durata,
        z.clasa,
        z.nr_locuri,
        z.pret
    from
        zboruri z,
        tari t,
        tari t1,
        orase o,
        orase o1
    where
        z.id_tara_plecare = id_tp and t.id = id_tp and
        z.id_oras_plecare = id_op and o.id = id_op and
        z.id_tara_sosire = id_ts and t1.id = id_ts and
        z.id_oras_sosire = id_os and o1.id = id_os and
        z.nr_locuri > 0 and
        (z.data_plecare > datap or (z.data_plecare = datap and z.ora_plecare > orap));
end $$
delimiter ;

delimiter $$
create function get_most_tickets()
returns int
deterministic
begin
    return (select max(aux.contor)
            from (select count(id_tara_sosire) contor
                  from (select z.id_tara_sosire, b.id_zbor from zboruri z, bilete b where z.ID = b.id_zbor) a group by id_tara_sosire) aux);
end $$
delimiter ;

delimiter $$
create procedure getMostVisitedCountry(out max_nr_visits int)
begin
    declare most_tickets int default get_most_tickets();

    set max_nr_visits = most_tickets;

    select nume from tari where ID in (select id_zbor from bilete group by id_zbor having count(id_zbor) = @most_tickets);
end $$
delimiter ;

-- SELECT
--   count(zboruri.id_tara_sosire) as numar_vizite,
--   tari.nume
-- FROM zboruri, bilete, tari
-- where
--   zboruri.id_tara_sosire = tari.ID and zboruri.ID = bilete.id_zbor
-- group by
--   zboruri.id_tara_sosire
-- order by numar_tari_vizitate desc

delimiter $$
create procedure insertClient(in cnp varchar(255), in username varchar(255),
                              in nume varchar(255), in prenume varchar(255),
                              in password varchar(255))
begin
        insert into clienti values(cnp, username, nume, prenume, password);
end $$
delimiter ;

delimiter $$
create procedure insertTicket(in id_z int, in id_c varchar(15))
begin
        insert into bilete (id_zbor, id_client) values(id_z, id_c);
end $$
delimiter ;

delimiter $$
create procedure getStates(in id_t int)
begin
        select id, nume from orase where id_tara = id_t;
end $$
delimiter ;
