delimiter $$
create trigger bilete_before_insert
before insert on bilete
for each row
begin
    declare nr_loc int;
    update
        zboruri
    set nr_locuri = nr_locuri - 1
    where
        id = new.id_zbor;
end $$
delimiter ;
