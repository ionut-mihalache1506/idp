docker swarm init --advertise-addr 127.0.0.1

docker build -t admin -f ./admin/Dockerfile admin
docker build -t admin_server -f ./admin_server/Dockerfile admin_server
docker build -t database -f ./database/Dockerfile database
docker build -t www -f ./phpserver/Dockerfile phpserver

docker stack deploy -c .\idp.yml ProAir

# docker stack rm ProAir
# docker swarm leave --force
