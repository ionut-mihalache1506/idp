#!/usr/bin/python

import socket
from datetime import datetime
from threading import Thread

HOST = 'admin_server'     # name of the server docker
PORT = 65432        # Port to listen on (non-privileged ports are > 1023)

RECV_BUFSIZE = 4096

admin_clients = {
    "admin1": "admin1",
    "admin2": "admin2",
    "admin3": "admin3",
    "admin4": "admin4",
    "admin5": "admin5"
}

def server_client(conn, addr):
    print("New connection from:", addr)
    username_query = "Username: "
    password_query = "Password: "
    wrong_username = "Username not recognized!"
    wrong_password = "Wrong password!"
    auth_success = "Authentication done!"
    welcome_message = "Welcome to server!\nGive login command to start authentication or logout to exit now!"

    # Send welcome message
    conn.send(welcome_message.encode())

    # Check command
    data = conn.recv(RECV_BUFSIZE)
    cmd = data.decode()
    if cmd == "logout":
        print(addr, "logged out!")
        return
    if cmd != "login":
        return

    while True:
        # Ask for username
        conn.send(username_query.encode())
        data = conn.recv(RECV_BUFSIZE)
        username = data.decode()
        if username not in admin_clients:
            conn.send(wrong_username.encode())
            continue

        # Ask for password
        conn.send(password_query.encode())
        data = conn.recv(RECV_BUFSIZE)
        password = data.decode()
        if admin_clients[username] != password:
            conn.send(wrong_password.encode())
            continue
        conn.send(auth_success.encode())

        # Create or open the admin file
        admin_client_file = open(username + ".txt", "a+")
        # Log client operations
        end = False
        while not end:
            data = conn.recv(RECV_BUFSIZE)
            cmd = data.decode()
            if cmd == "logout" or cmd == "exit":
                admin_client_file.close()
                end = True
                print(addr, "logged out!")
                break
            now = datetime.now()
            dt_string = now.strftime("%d-%m-%Y %H:%M:%S")
            admin_client_file.write(username + " (" + dt_string + ")" + " -> " + cmd + '\n')
        if end:
            break
    conn.close()

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()

    while True:
        conn, addr = s.accept()
        with conn:
            print(conn)
            server_client(conn, addr)

    s.close()
